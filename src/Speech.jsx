import React, { Component } from "react"

//------------------------SPEECH RECOGNITION-----------------------------

const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
const recognition = new SpeechRecognition()

recognition.continous = true
recognition.interimResults = true
// recognition.lang = 'pt-BR' // Aparentemente mesmo em pr-BR ele continua reconhecendo en-US, o mesmo comportamento acontece se deixar sem definição
// https://appmakers.dev/bcp-47-language-codes-list/

//---LOG---

function LOG(...text) {
    const elem = document.getElementById('log')
    if (!elem) return;
    elem.innerText += text
    elem.innerHTML += "<br />"
    elem.scrollTop = elem.scrollHeight - elem.clientHeight;
}

recognition.onaudiostart = e => {
    LOG("audiostart")
}
recognition.onaudioend = e => {
    LOG("audioend")
}
recognition.onsoundstart = e => {
    LOG("soundstart")
}
recognition.onsoundend = e => {
    LOG("soundend")
}
recognition.onspeechstart = e => {
    LOG("speechstart")
}
recognition.onspeechend = e => {
    LOG("speechend")
}


//------------------------COMPONENT-----------------------------

class Speech extends Component {

    constructor() {
        super()
        this.state = {
            listening: false
        }
        this.toggleListen = this.toggleListen.bind(this)
        this.handleListen = this.handleListen.bind(this)
    }

    componentDidMount() {
        LOG('I\'m asking for mic permission')
        const btn = document.getElementById('microphone-btn')
        btn.disabled = true
        navigator.mediaDevices.getUserMedia({ audio: true })
            .then(function (stream) {
                LOG('You let me use your mic!')
                stream.getTracks().forEach(function (track) {
                    track.stop();
                });

                btn.disabled = false
                btn.innerText = 'Start'
            })
            .catch(function (err) {
                LOG('You blocked me to use your mic!')
                btn.innerText = 'Sad'
            });
    }

    toggleListen() {
        this.setState({
            listening: !this.state.listening
        }, this.handleListen)
    }

    handleListen() {

        LOG('listening?', this.state.listening)

        if (this.state.listening) {
            recognition.start()
            recognition.onend = () => {
                LOG("...continue listening...")
                recognition.start()
            }

        } else {
            recognition.stop()
            recognition.onend = () => {
                LOG("Stopped listening per click")
                document.getElementById('microphone-btn').innerText = "Start"
            }
        }

        recognition.onstart = () => {
            LOG("Listening!")
            document.getElementById('microphone-btn').innerText = "Stop"
        }

        let finalTranscript = ''
        recognition.onresult = event => {
            let interimTranscript = ''

            for (let i = event.resultIndex; i < event.results.length; i++) {
                const transcript = event.results[i][0].transcript;
                if (event.results[i].isFinal) finalTranscript += transcript + ' ';
                else interimTranscript += transcript;
            }
            const interim = document.getElementById('interim')
            interim.innerHTML = interimTranscript
            interim.scrollLeft = interim.scrollWidth - interim.clientWidth;

            document.getElementById('final').innerHTML = finalTranscript

            //-------------------------COMMANDS------------------------------------

            const transcriptArr = finalTranscript.split(' ')
            const stopCmd = transcriptArr.slice(-4, -1)
            console.log(transcriptArr, stopCmd)
            LOG('stopCmd', stopCmd)

            if (/^pare de ouvir$/i.test(stopCmd.join(' '))) {
                recognition.stop()
                recognition.onend = () => {
                    LOG('Stopped listening per command')
                    const finalText = transcriptArr.slice(0, -4).join(' ')
                    document.getElementById('final').innerHTML = finalText
                    document.getElementById('microphone-btn').innerText = "Start"
                }
            }
        }

        //-----------------------------------------------------------------------

        recognition.onerror = event => {
            LOG("Error occurred in recognition: " + event.error)
        }

    }

    render() {
        return (
            <div style={container}>
                <button
                    id='microphone-btn'
                    style={button}
                    onClick={this.toggleListen}
                >
                    Wait
                </button>
                <p>Diga "pare de ouvir" no final para parar com sua voz.</p>
                <div id='interim' style={interim}></div>
                <div id='final' style={final}></div>
                <div id='log' style={log}></div>
            </div>
        )
    }
}

export default Speech


//-------------------------CSS------------------------------------

const styles = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'center'
    },
    button: {
        width: '60px',
        height: '60px',
        background: 'lightblue',
        borderRadius: '50%',
        margin: '6em 0 2em 0'
    },
    interim: {
        color: 'gray',
        border: '#ccc 1px solid',
        padding: '1em',
        paddingRight: '150px',
        boxSizing: 'border-box',
        textAlign: 'right',
        margin: '1em',
        width: 'calc(300px + 2em + 2px)',

        'white-space': 'nowrap',
        overflow: 'hidden'
    },
    final: {
        color: 'black',
        border: '#ccc 1px solid',
        padding: '1em',
        margin: '1em',
        width: '300px'
    },
    log: {
        color: 'black',
        border: '#ccc 1px solid',
        padding: '1em',
        margin: '1em',
        width: '300px',
        height: '200px',
        overflow: 'auto'
    }
}

const { container, button, interim, final, log } = styles